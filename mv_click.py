#import pyautogui
#import time
#import keyboard
##for i in range(10):
#while True:
#    #pyautogui.moveTo(100,100)
#    pyautogui.click()
#    time.sleep(10)
#    #print("click", i ,"times")
#    print("click time",time.ctime())
#
#    if keyboard.is_pressed('q'):
#        print("keyboard q detected,quit")
#        break

import pyautogui
import time
import keyboard

def auto_clicker():
    """
    该函数实现自动点击器功能。
    每隔10秒检查一次是否需要自动点击屏幕，并在按下'q'键时立即终止程序。
    """
    try:
        click_time = time.time()  # 记录上次点击时间
        while True:
            # 每次循环开始时检查是否按下'q'键
            if keyboard.is_pressed('q'): 
                print("Keyboard 'q' detected, quitting...")
                break
            
            # 计算距离上次点击是否已超过10秒
            elapsed = time.time() - click_time
            if elapsed >= 10:
                pyautogui.click()  # 达到时间间隔后点击
                print(f"Click time: {time.ctime()}")
                click_time = time.time()  # 更新上次点击时间
            
            # 稍作延时减少CPU占用，但确保能及时响应'q'
            time.sleep(0.1)
            
    except KeyboardInterrupt:
        print("\nManual interruption detected. Exiting...")

if __name__ == "__main__":
    auto_clicker()
